#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#

import unittest.mock

from requests import ConnectionError, Request
import pytest

from jenkins_lockable_resources.app import INFO_STATE_COLORS

from conftest import RESOURCES

no_match_param = pytest.mark.parametrize(
    "name, label, expected",
    [
        (r"^dummy.*", None, "No resources matching name~=/^dummy.*/"),
        (None, r"dummy", "No resources matching label~=/dummy/"),
        (
            r"^dummy.*",
            r"dummy",
            "No resources matching name~=/^dummy.*/ or label~=/dummy/",
        ),
    ],
    ids=["name", "label", "name+label"],
)


def resource_control_assert(name, streamer, jenkins, resources):
    streamer.error.assert_not_called()
    if resources:
        action = name[0].upper() + name[1:] + "d"
        streamer.info.assert_has_calls(
            (unittest.mock.call(f"{action} {r}") for r in resources)
        )
        jenkins.requester.post_url.assert_has_calls(
            (
                unittest.mock.call(
                    f"{jenkins.baseurl}/lockable-resources/{name}?resource={r}"
                )
                for r in resources
            ),
            any_order=True
        )
    else:
        streamer.info.assert_not_called()
        jenkins.requester.post_url.assert_not_called()


def reserve_assert(streamer, jenkins, *resources):
    resource_control_assert("reserve", streamer, jenkins, resources)


def test_reserve(app, streamer, jenkins):
    jenkins.username = "MrBean"
    app.reserve()
    reserve_assert(streamer, jenkins, "skiny.domain.com")


def test_reserve_multiple(app, streamer, jenkins):
    jenkins.username = "MrBean"
    app.reserve(r".*")
    reserve_assert(streamer, jenkins, "skiny.domain.com", "bobby.domain.com")


def test_reserve_no_free(monkeypatch, app, streamer, lockable_resources, jenkins):
    def fake_poll(*args, **kw):
        return {
            "resources": [
                {"name": "skiny.domain.com", "state": "RESERVED", "owner": "MrBean"},
                {"name": "bobby.domain.com", "state": "RESERVED", "owner": "MrBean"},
            ]
        }

    monkeypatch.setattr(type(lockable_resources), "_poll", fake_poll)

    app.reserve()
    reserve_assert(streamer, jenkins)
    streamer.warn.assert_called_once_with(
        "Sorry, no free resources at the moment. Try again later."
    )


def test_reserve_another_resource_forced(app, streamer, jenkins):
    app.reserve(force=True)
    reserve_assert(streamer, jenkins, "skiny.domain.com")
    streamer.warn.assert_called_once_with("You already have one resource owned.")
    streamer.confirm.assert_not_called()


def test_reserve_another_resource_not_forced(app, streamer, jenkins):
    app.reserve(force=False)
    reserve_assert(streamer, jenkins)
    streamer.warn.assert_called_once_with("You already have one resource owned.")
    streamer.confirm.assert_not_called()


def test_reserve_another_resource_user_confirmed(app, streamer, jenkins):
    streamer.confirm = unittest.mock.MagicMock(return_value=True)
    app.reserve()
    reserve_assert(streamer, jenkins, "skiny.domain.com")
    streamer.warn.assert_called_once_with("You already have one resource owned.")
    streamer.confirm.assert_called_once_with("Force reserving?")


def test_reserve_another_resource_user_rejected(app, streamer, jenkins):
    streamer.confirm = unittest.mock.MagicMock(return_value=False)
    app.reserve()
    reserve_assert(streamer, jenkins)
    streamer.warn.assert_called_once_with("You already have one resource owned.")
    streamer.confirm.assert_called_once_with("Force reserving?")


@no_match_param
def test_reserve_no_match(app, streamer, jenkins, name, label, expected):
    app.reserve(name=name, label=label)
    streamer.error.assert_called_once_with(expected)


def unreserve_assert(streamer, jenkins, *resources):
    resource_control_assert("unreserve", streamer, jenkins, resources)


def test_unreserve(app, streamer, jenkins):
    app.unreserve()
    unreserve_assert(streamer, jenkins, "fluffy.domain.com")


def test_unreserve_multiple(monkeypatch, app, streamer, lockable_resources, jenkins):
    def fake_poll(*args, **kw):
        return {
            "resources": [
                {"name": "skiny.domain.com", "state": "RESERVED", "owner": "MrBean"},
                {"name": "bobby.domain.com", "state": "RESERVED", "owner": "McGiver"},
                {"name": "fluffy.domain.com", "state": "RESERVED", "owner": "McGiver"},
                {
                    "name": "fatty.domain.com",
                    "state": "LOCKED",
                    "owner": "KnightOfNightly",
                },
            ]
        }

    monkeypatch.setattr(type(lockable_resources), "_poll", fake_poll)

    app.unreserve(r".*")
    unreserve_assert(streamer, jenkins, "bobby.domain.com", "fluffy.domain.com")


def test_unreserve_not_reserved(app, streamer, jenkins):
    jenkins.username = "MrBean"
    app.unreserve()
    unreserve_assert(streamer, jenkins)
    streamer.warn.assert_called_once_with("No resources to release")


@no_match_param
def test_unreserve_no_match(app, streamer, jenkins, name, label, expected):
    app.unreserve(name=name, label=label)
    streamer.error.assert_called_once_with(expected)


def test_list(app, streamer, jenkins):
    app.list()
    streamer.error.assert_not_called()
    streamer.info.assert_has_calls((unittest.mock.call(r["name"]) for r in RESOURCES))


def test_list_short_name(app, streamer, jenkins):
    app.list(short_name=True)
    streamer.error.assert_not_called()
    streamer.info.assert_has_calls(
        (unittest.mock.call(r["name"].split(".")[0]) for r in RESOURCES)
    )


def test_info(app, streamer, jenkins):
    app.info()
    streamer.error.assert_not_called()
    calls = []
    for res in RESOURCES:
        state = res["state"]
        info = state
        if not state == "FREE":
            info += " by {owner}".format(**res)
        calls.append(unittest.mock.call("{name}".format(**res), nl=False))
        if res["label"]:
            calls.append(unittest.mock.call(" [{label}]".format(**res), nl=False))
        calls.append(unittest.mock.call(": ".format(**res), nl=False))
        calls.append(unittest.mock.call(info, fg=INFO_STATE_COLORS[state]))
    assert streamer.info.call_args_list == calls


def test_owned(app, streamer):
    app.owned("McGiver")
    streamer.error.assert_not_called()
    streamer.info.assert_called_once_with("fluffy.domain.com")


def test_owned_short_name(app, streamer):
    app.owned("McGiver", True)
    streamer.error.assert_not_called()
    streamer.info.assert_called_with("fluffy")


def test_owned_reserve_if_not_owned(app, streamer):
    app.owned("MrBean", reserve=True)
    streamer.error.assert_not_called()
    streamer.info.assert_called_once_with("skiny.domain.com")


def test_owned_multiple_select_index(monkeypatch, lockable_resources, app, streamer):
    def fake_poll(*args, **kw):
        return {
            "resources": [
                {"name": "skiny.domain.com", "state": "RESERVED", "owner": "MrBean"},
                {"name": "bobby.domain.com", "state": "RESERVED", "owner": "McGiver"},
                {"name": "fluffy.domain.com", "state": "RESERVED", "owner": "McGiver"},
                {
                    "name": "fatty.domain.com",
                    "state": "LOCKED",
                    "owner": "KnightOfNightly",
                },
            ]
        }

    monkeypatch.setattr(type(lockable_resources), "_poll", fake_poll)

    app.owned(index=1)
    streamer.error.assert_not_called()
    streamer.info.assert_called_once_with("fluffy.domain.com")


@pytest.mark.parametrize(
    "exception,message",
    [
        # Exception                                   Expected message
        (
            ConnectionError(request=Request(url="URL")),
            "Failed to connect to URL. Check your connection and try again.",
        ),
        (Exception("An exception"), "An exception"),
    ],
    ids=["ConnectionError", "Exception"],
)
def test_api_connection_exception(
    monkeypatch, app, lockable_resources, streamer, jenkins, exception, message
):
    def exception_poll(*args, **kw):
        raise exception

    monkeypatch.setattr(type(lockable_resources), "_poll", exception_poll)
    app.reserve()
    streamer.error.assert_called_once_with(message)
