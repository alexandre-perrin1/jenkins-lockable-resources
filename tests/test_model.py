#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#

import unittest.mock

import pytest
from requests import RequestException

from conftest import RESOURCES, RESERVED_RESOURCES, FREE_RESOURCES


def test_str(lockable_resources, jenkins):
    assert str(lockable_resources) == jenkins.baseurl + "/lockable-resources"


def test_list_resources(lockable_resources, jenkins):
    resources = lockable_resources.list_resources()
    assert resources == [res["name"] for res in RESOURCES]


def test_get_resources(lockable_resources, jenkins):
    resources = lockable_resources.get_resources()
    assert len(lockable_resources) == len(RESOURCES)
    assert len(resources) == len(RESOURCES)

    for res, expected in zip(resources, RESOURCES):
        assert expected["name"] == str(res)
        assert str(expected) == repr(res)


def test_get_owned_resources(lockable_resources, jenkins):
    owned_resources = [r for r in RESERVED_RESOURCES if r["owner"] == jenkins.username]
    resources = lockable_resources.get_owned_resources()
    assert len(resources) == len(owned_resources)

    for res, expected in zip(resources, owned_resources):
        assert expected["name"] == str(res)
        assert str(expected) == repr(res)


def test_get_free_resources(lockable_resources, jenkins):
    resources = lockable_resources.get_free_resources()
    assert len(resources) == len(FREE_RESOURCES)

    for res, expected in zip(resources, FREE_RESOURCES):
        assert expected["name"] == str(res)
        assert str(expected) == repr(res)


def test_items(lockable_resources, jenkins):
    items = list(lockable_resources.items())
    assert len(items) == len(RESOURCES)

    for (_, res), expected in zip(items, RESOURCES):
        assert expected["name"] == str(res)
        assert str(expected) == repr(res)


def test_values_match_name(lockable_resources, jenkins):
    values = list(lockable_resources.values(name=r"^f.*"))
    expected_values = [r for r in RESOURCES if r["name"].startswith("f")]
    assert len(values) == 2

    for res, expected in zip(values, expected_values):
        assert expected["name"] == str(res)
        assert str(expected) == repr(res)


def test_values_match_label(lockable_resources, jenkins):
    values = list(lockable_resources.values(label=r"label1"))
    expected_values = [r for r in RESOURCES if r["label"] == "label1"]
    assert len(values) == 2

    for res, expected in zip(values, expected_values):
        assert expected["label"] == res.label
        assert str(expected) == repr(res)


def test_values_match_status(lockable_resources, jenkins):
    values = list(lockable_resources.values(state="reserved"))
    expected_values = [r for r in RESOURCES if r["state"] == "RESERVED"]
    assert len(values) == len(expected_values)

    for res, expected in zip(values, expected_values):
        assert expected["state"] == res.state
        assert str(expected) == repr(res)


def test_contains(resource, lockable_resources, jenkins):
    name = resource["name"]
    assert name in lockable_resources


def test_get_owner(reserved, lockable_resources, jenkins):
    name = reserved["name"]
    owner = lockable_resources.get_owner(name)
    assert owner == reserved["owner"]


def test_reserve_free(free, lockable_resources, jenkins):
    name = free["name"]
    lockable_resources.reserve(name)
    jenkins.requester.post_url.assert_called_once_with(
        "{}/lockable-resources/reserve?resource={}".format(jenkins.baseurl, name)
    )


def test_unreserve_reserved(reserved, lockable_resources, jenkins):
    name = reserved["name"]
    lockable_resources.unreserve(name)
    jenkins.requester.post_url.assert_called_once_with(
        "{}/lockable-resources/unreserve?resource={}".format(jenkins.baseurl, name)
    )


def test_is_reserved(resource, lockable_resources, jenkins):
    name = resource["name"]
    reserved = resource["state"] == "RESERVED"
    assert reserved == lockable_resources.is_reserved(name)


def test_is_free(resource, lockable_resources, jenkins):
    name = resource["name"]
    reserved = resource["state"] == "FREE"
    assert reserved == lockable_resources.is_free(name)


def test_resource_is_free(lockable_resources, jenkins):
    resources = lockable_resources.get_resources()
    for resource in resources:
        free = resource.state == "FREE"
        assert free == resource.is_free()


def test_resource_is_reserved(lockable_resources, jenkins):
    resources = lockable_resources.get_resources()
    for resource in resources:
        reserved = resource.state == "RESERVED"
        assert reserved == resource.is_reserved()


def test_resource_is_locked(lockable_resources, jenkins):
    resources = lockable_resources.get_resources()
    for resource in resources:
        locked = resource.state == "LOCKED"
        assert locked == resource.is_locked()


def test_resource_is_owned(reserved, lockable_resources, jenkins):
    resources = lockable_resources.get_resources()
    for resource in resources:
        owned = resource.owner == jenkins.username
        assert owned == resource.is_owned()


def test_resource_not_found(reserved, lockable_resources, jenkins):
    with pytest.raises(KeyError):
        _ = lockable_resources["dummy"]


HTML_DATA = """
<html>
    <div id="main-panel">
        <table class="pane">
            <tr data-resource-name="skiny.domain.com">
                <td class="pane">
                    <strong>skiny.domain.com</strong>
                    <br>
                    <em></em>
                </td>
                <td class="pane" style="color: green;">
                    <strong>FREE</strong>
                </td>
                <td class="pane">label1</td>
                <td class="pane">false</td>
                <td class="pane"></td>
            </tr>
            <tr data-resource-name="bobby.domain.com">
                <td class="pane">
                    <strong>bobby.domain.com</strong>
                    <br>
                    <em></em>
                </td>
                <td class="pane" style="color: green;">
                    <strong>FREE</strong>
                </td>
                <td class="pane">label2</td>
                <td class="pane">false</td>
                <td class="pane"></td>
            </tr>
            <tr data-resource-name="fluffy.domain.com">
                <td class="pane">
                    <strong>fluffy.domain.com</strong>
                    <br>
                    <em></em>
                </td>
                <td class="pane">
                    <strong>RESERVED</strong>
                    by
                    <strong>McGiver</strong>
                </td>
                <td class="pane">label1</td>
                <td class="pane">false</td>
                <td class="pane"></td>
            </tr>
            <tr data-resource-name="fatty.domain.com">
                <td class="pane">
                    <strong>fatty.domain.com</strong>
                    <br>
                    <em></em>
                </td>
                <td class="pane">
                    <strong>LOCKED</strong>
                    by
                    <strong>KnightOfNightly</strong>
                </td>
                <td class="pane">label2</td>
                <td class="pane">false</td>
                <td class="pane"></td>
            </tr>
        </table>
    </div>
</html>
"""


def test_data_interface(mocker, lockable_resources, jenkins):
    request = mocker.MagicMock()
    request.ok = True
    request.text = HTML_DATA
    jenkins.requester.post_url = lambda u: request

    data_html = lockable_resources.get_data(jenkins.baseurl)
    data_test = lockable_resources.poll()

    assert data_test == data_html


def test_data_interface_failure(mocker, lockable_resources, jenkins):
    request = mocker.MagicMock()
    request.ok = False

    def raise_except():
        raise RequestException

    request.raise_for_status = raise_except
    jenkins.requester.post_url = lambda u: request

    with pytest.raises(RequestException):
        lockable_resources.get_data(jenkins.baseurl)
