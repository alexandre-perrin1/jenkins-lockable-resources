import fnmatch

import pytest
import click

from jenkins_lockable_resources import VERSION
from jenkins_lockable_resources.main import default_streamer

from conftest import FAKE_JENKINS_URL, cli_entry

CLI_BASE_ARGS = ["--jenkins-url", FAKE_JENKINS_URL]
CLI_MRBEAN_USER = ["--jenkins-user", "MrBean", "--jenkins-token", "FAKE_TOKEN"]
CLI_MCGIVER_USER = ["--jenkins-user", "McGiver", "--jenkins-token", "FAKE_TOKEN"]


def cli_parametrize(args):
    return pytest.mark.parametrize(
        "args, expected", args, ids=[" ".join(a[0]) for a in args]
    )


def test_version(cli):
    result = cli("--version")
    assert VERSION in result.output


def test_context(cli, app_fact_mock):
    cli(*CLI_BASE_ARGS, *CLI_MCGIVER_USER, "--filter", ".*", "reserve")
    app_fact_mock.assert_called_once_with(
        default_streamer, FAKE_JENKINS_URL, "McGiver", "FAKE_TOKEN", ".*", True
    )


def test_context_no_interactive(cli, app_fact_mock):
    cli(*CLI_BASE_ARGS, *CLI_MCGIVER_USER, "--no-interactive", "reserve")
    app_fact_mock.assert_called_once_with(
        default_streamer, FAKE_JENKINS_URL, "McGiver", "FAKE_TOKEN", None, False
    )


def test_context_prompt(cli, app_fact_mock):
    cli(*CLI_BASE_ARGS, "reserve", input="McGiver\nFAKE_TOKEN\n")
    app_fact_mock.assert_called_once_with(
        default_streamer, FAKE_JENKINS_URL, "McGiver", "FAKE_TOKEN", None, True
    )


@cli_parametrize(
    [
        ((), (None, None, None)),
        (("*",), (fnmatch.translate("*"), None, None)),
        (("--regex", "EXPR"), ("EXPR", None, None)),
        (("-e", "EXPR"), ("EXPR", None, None)),
        (("--label", "EXPR"), (None, "EXPR", None)),
        (("-L", "EXPR"), (None, "EXPR", None)),
        (("--force",), (None, None, True)),
        (("-f",), (None, None, True)),
    ]
)
def test_reserve(cli, app_mock, args, expected):
    cli(*CLI_BASE_ARGS, *CLI_MCGIVER_USER, "reserve", *args)
    app_mock.reserve.assert_called_once_with(*expected)


def test_reserve_not_forced(cli, app_mock):
    cli(*CLI_BASE_ARGS, *CLI_MCGIVER_USER, "--no-interactive", "reserve")
    app_mock.reserve.assert_called_once_with(None, None, False)


@cli_parametrize(
    [
        ((), (None, None)),
        (("*",), (fnmatch.translate("*"), None)),
        (("--regex", "EXPR"), ("EXPR", None)),
        (("-e", "EXPR"), ("EXPR", None)),
        (("--label", "EXPR"), (None, "EXPR")),
        (("-L", "EXPR"), (None, "EXPR")),
    ]
)
def test_unreserve(cli, app_mock, args, expected):
    cli(*CLI_BASE_ARGS, *CLI_MCGIVER_USER, "unreserve", *args)
    app_mock.unreserve.assert_called_once_with(*expected)


@cli_parametrize(
    [
        ((), (None, None, None, False)),
        (("*",), (fnmatch.translate("*"), None, None, False)),
        (("--regex", "EXPR"), ("EXPR", None, None, False)),
        (("-e", "EXPR"), ("EXPR", None, None, False)),
        (("--label", "EXPR"), (None, "EXPR", None, False)),
        (("-L", "EXPR"), (None, "EXPR", None, False)),
        (("--state", "free"), (None, None, "free", False)),
        (("--state", "reserved"), (None, None, "reserved", False)),
        (("--state", "locked"), (None, None, "locked", False)),
        (("-S", "free"), (None, None, "free", False)),
        (("--short-name",), (None, None, None, True)),
        (("-s",), (None, None, None, True)),
    ]
)
def test_list(cli, app_mock, args, expected):
    cli(*CLI_BASE_ARGS, *CLI_MCGIVER_USER, "list", *args)
    app_mock.list.assert_called_once_with(*expected)


@cli_parametrize(
    [
        ((), (None, None, None)),
        (("*",), (fnmatch.translate("*"), None, None)),
        (("--regex", "EXPR"), ("EXPR", None, None)),
        (("-e", "EXPR"), ("EXPR", None, None)),
        (("--label", "EXPR"), (None, "EXPR", None)),
        (("-L", "EXPR"), (None, "EXPR", None)),
        (("--state", "free"), (None, None, "free")),
        (("--state", "reserved"), (None, None, "reserved")),
        (("--state", "locked"), (None, None, "locked")),
        (("-S", "free"), (None, None, "free")),
    ]
)
def test_info(cli, app_mock, args, expected):
    cli(*CLI_BASE_ARGS, *CLI_MCGIVER_USER, "info", *args)
    app_mock.info.assert_called_once_with(*expected)


@cli_parametrize(
    [
        ((), (None, False, None, None, False, None, None)),
        (("--user", "McGiver"), ("McGiver", False, None, None, False, None, None)),
        (("--short-name",), (None, True, None, None, False, None, None)),
        (("--count", "1"), (None, False, 1, None, False, None, None)),
        (("--index", "1"), (None, False, None, 1, False, None, None)),
        (("--reserve",), (None, False, None, None, True, None, None)),
        (
            ("--reserve-pattern", "*"),
            (None, False, None, None, False, fnmatch.translate("*"), None),
        ),
        (("--reserve-regex", ".*"), (None, False, None, None, False, ".*", None)),
        (("--reserve-label", "label"), (None, False, None, None, False, None, "label")),
    ]
)
def test_owned(cli, app_mock, args, expected):
    cli(*CLI_BASE_ARGS, *CLI_MCGIVER_USER, "owned", *args)
    app_mock.owned.assert_called_once_with(*expected)


def test_launch(mocker, monkeypatch, cli, app_mock):
    monkeypatch.setattr(click, "launch", mocker.MagicMock())
    app_mock.obj.baseurl = "FAKE"
    cli(*CLI_BASE_ARGS, *CLI_MCGIVER_USER, "launch")
    click.launch.assert_called_once_with("FAKE", wait=False)
